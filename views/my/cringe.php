<?php


/** @var yii\web\View $this */
/** @var string $skill */



use yii\helpers\Html;

$this->title = 'style';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $skill ?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
