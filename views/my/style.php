<?php


/** @var yii\web\View $this */
/** @var string $nazvanie */


use yii\helpers\Html;

$this->title = 'style';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$nazvanie?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
