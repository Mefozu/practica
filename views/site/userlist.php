<?php

use app\models\User;
?>
<div class="Status">
    <h1><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'status']); ?>
            <tr>
                <td>
                    Name: <?=$model->username?>
                </td>
                <td>
                    Email: <?=$model->email?>
                </td>
                <td>
                    <a class="btn btn-primary" href="<?=Url::to(['/country/updstatus', 'id' => $model->id])?>">
                        Deactivate
                    </a>
                </td>
            </tr>

            <?php ActiveForm::end(); ?>
</div>